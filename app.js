import nats from "nats";
import jsonata from "jsonata";

class AttributeFilterException extends Error {}
class AttributeFilterFailedException extends AttributeFilterException {}
class AttributeTransformationException extends Error {}
class AttributeValidationException extends Error {}

(async () => {
  const connection = await nats.connect({ servers: "nats" });

  const codec = nats.JSONCodec()

  async function rpc_register(func) {
    connection.subscribe(`structure.rpc.${func.name}`, {
      callback: async (error, msg) => {
        const payload = codec.decode(msg.data);
        let response = undefined;
        try {
          response = {
            result: await func(...Object.values(payload)),
          };
        } catch (e) {
          response = {
            exception: {
              type: e.constructor.name,
              message: e.message,
              args: [e.message],
              stack: [],
            },
          };
        }

        connection.publish(msg.reply, codec.encode(response));
      },
    });
  }

  function get_jsonata(attribute, key) {
    return (attribute[key] ?? [])
      .map((item) => item.jsonata)
      .filter((item) => item && item.trim());
  }

  async function jsonata_calculate_display(attributes) {
    for (const attribute of attributes) {
      try {
        const transformations_display = get_jsonata(
          attribute,
          "transformation_display"
        );
        attribute.value_display = attribute.value;
        for (const transformation_display of transformations_display) {
          attribute.value_display = await jsonata(
            transformation_display
          ).evaluate(attribute.value_display);
        }
      } catch (e) {
        console.log(
          `error generating display value for attribute ${attribute.key}: ${e}`
        );
      }
    }
    return attributes;
  }

  async function jsonata_verify(attribute, value) {
    let value_json = undefined;
    const get_value_json = () => {
      if (value_json == undefined) {
        try {
          value_json = JSON.parse(value);
        } catch (e) {
          throw new AttributeFilterException(`Cannot parse ${value} as JSON`);
        }
      }
      return value_json;
    };

    const bindings = {
      old: attribute.value,
    };

    for (const filter of get_jsonata(attribute, "filter")) {
      let expression = undefined;
      try {
        expression = jsonata(filter);
      } catch (e) {
        throw new AttributeFilterException(e.message);
      }

      if (!(await expression.evaluate(get_value_json(), bindings))) {
        throw new AttributeFilterFailedException(`Filter query not met`);
      }
    }

    attribute.valid = true;
    const validations = get_jsonata(attribute, "validation");
    for (const validation of validations) {
      try {
        attribute.valid = Boolean(
          await jsonata(validation).evaluate(get_value_json(), bindings)
        );
        if (!attribute.valid) {
          // logical and short circuiting
          break;
        }
      } catch (e) {
        throw new AttributeValidationException(e.message);
      }
    }

    const transformations = get_jsonata(attribute, "transformation");
    for (const transformation of transformations) {
      // check if value is parsable at all
      get_value_json();
      try {
        value_json = await jsonata(transformation).evaluate(
          value_json,
          bindings
        );
        value = value_json;
      } catch (e) {
        throw new AttributeTransformationException(e.message);
      }
    }

    attribute.key = attribute.id;
    attribute.value = value;

    return attribute;
  }

  rpc_register(jsonata_verify);
  rpc_register(jsonata_calculate_display);

  console.log("started");
})();
